import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html'
})
export class ModalComponent {

  visible = false;
  fadeShow = false;

  @Input() canHide = true;
  @Input() modalTitle: string;
  @Input() modalSubTitle: string;
  @Input() large = false;
  @Input() extraLarge = false;
  @Input() centered = true;
  @Input() scrollModal = false;

  show() {
    this.visible = true;
    setTimeout(() => this.fadeShow = true, 50);
  }

  hide() {
    this.fadeShow = false;
    setTimeout(() => this.visible = false, 150);
  }

  hideIfCan() {
    if (this.canHide) {
      this.hide();
    }
  }

  isVisible() {
    return this.visible;
  }

  stopPropagation(event) {
    event.stopPropagation();
  }

}
