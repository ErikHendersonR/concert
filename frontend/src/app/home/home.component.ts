import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalComponent } from '../shared/modal.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  checkbox:any;

  @ViewChild(ModalComponent)
  private modalComponent: ModalComponent;

  constructor() { }

  ngOnInit() {
    setTimeout(() => this.show(), 250);
  }

  async show() {
    this.modalComponent.canHide = false;
    this.modalComponent.show();
  }

  public checked() {
    this.checkbox = !this.checkbox;
  }

  setCompetencia() {
    this.modalComponent.hide();

    // this.router.navigate(['/']);
  }
}
